'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const { validate } = use('Validator');
const HarryPotter = use("App/Models/HarryPotter");
const HarryPotterException = use('App/Exceptions/HarryPotterException');
const Axios = use('axios'); 
const {isEmpty} = use('lodash');
const Env = use('Env');

/**
 * Resourceful controller for interacting with harrypotters
 */
class HarryPotterController {
  /**
   * Show a list of all harrypotters.
   * GET harrypotters
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({request, response, view }) {
    if (!isEmpty(request.get())) {
      return await HarryPotter.query().where(request.get()).fetch();
    } 

    return await HarryPotter.all();    
  }

  /**
   * Create/save a new harrypotter.
   * POST harrypotters
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {

    const rules = {
      name: 'required',
      role: 'required',
      school: 'required',
      house: 'required',
      patronus: 'required',
    }

    const messages = {
      'name.required' : 'Campo obrigatório',
      'role.required'    : 'Campo obrigatório',
      'school.required'    : 'Campo obrigatório',
      'house.required'    : 'Campo obrigatório',
      'patronus.required'    : 'Campo obrigatório',
    }

    const validation = await validate(request.all(), rules, messages);

    if (validation.fails()) {
      throw new HarryPotterException(validation.messages(), 412)
    }

    await this.connectPotterApi(request.input('house'));
    
    const data = request.all();
    const harryPotter = await HarryPotter.create(data);
    return harryPotter;
  }

  /**
   * Display a single harrypotter.
   * GET harrypotters/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
    const harryPotter = await HarryPotter.findOrFail(params.id);

    return harryPotter;
  }

  /**
   * Update harrypotter details.
   * PUT or PATCH harrypotters/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    const harryPotter = await HarryPotter.findOrFail(params.id);
    const data = request.all();
    
    harryPotter.merge(data);
    await harryPotter.save();
   
    return harryPotter;
  }

  /**
   * Delete a harrypotter with id.
   * DELETE harrypotters/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
    const harryPotter = await HarryPotter.findOrFail(params.id);
    
    return await harryPotter.delete();

  }

  async connectPotterApi(params) {
    const potterApi = await Axios.get(`https://www.potterapi.com/v1/houses/${params}?key=$2a$10$YwcXYx8OXGCLr6fExIvM3uLL0TTfR7/.zUCZ4rbugtFRikzIm/at6`)
    .then((result) => {
      return result.data[0]
    })
    
    if (isEmpty(potterApi)) {
      throw new HarryPotterException('Id casa inválido', 412)
    }
    
    return potterApi;
  }
}

module.exports = HarryPotterController
