'use strict'

const { LogicalException } = require('@adonisjs/generic-exceptions')

class HarryPotterException extends LogicalException {
  /**
   * Handle this exception by itself
   */
  handle (error, { response }) {
    response
      .status(500)
      .send('Custom exception handled!')
  }
}

module.exports = HarryPotterException
