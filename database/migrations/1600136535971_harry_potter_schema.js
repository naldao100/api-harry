'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class HarryPotterSchema extends Schema {
  up () {
    this.dropIfExists('harry_potters')
    this.create('harry_potters', (table) => {
      table.increments()
      table.string('name')
      table.string('role')
      table.string('school')
      table.string('house')
      table.string('patronus')
      table.timestamps()
    })
  }

  down () {
    this.dropIfExists('harry_potters')
  }
}

module.exports = HarryPotterSchema
