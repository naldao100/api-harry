'use strict'

const { test, trait } = use('Test/Suite')('Harry Potter')

const HarryPotter = use('App/Models/HarryPotter')

trait('Test/ApiClient')

test('Retornar a listagem', async ({ client }) => {
  await HarryPotter.create({
    name: "Harry Potter",
    role: "student",
    school: "Hogwarts School of Witchcraft and Wizardry",
    house: "5a05e2b252f721a3cf2ea33f",
    patronus: "stag"
  })

  const response = await client.get('/harry-potter').end()

  response.assertStatus(200)
  response.assertJSONSubset([{
    name: "Harry Potter",
    role: "student",
    school: "Hogwarts School of Witchcraft and Wizardry",
    house: "5a05e2b252f721a3cf2ea33f",
    patronus: "stag"
  }])
})

test('Caindo em validacao de campo', async ({ client }) => {
  const response = await client
  .post('/harry-potter')
  .send({
    name: "Harry Potter",
    role: "student",
    school: "Hogwarts School of Witchcraft and Wizardry",
    house: "5a05e2b252f721a3cf2ea33f",
  }).end()

  response.assertError([
    {
      message: 'Campo obrigatório',
      field: 'patronus',
      validation: 'required'
    }
  ])

})

test('Id da casa inválido', async ({ client }) => {
  const response = await client
  .post('/harry-potter')
  .send({
    name: "Harry Potter",
    role: "student",
    school: "Hogwarts School of Witchcraft and Wizardry",
    house: "5a05e2b252f721a3cf2ea33",
    patronus: "stag"
  }).end()

  response.assertError('Id casa inválido')

})
