# Api Harry

API REST desenvolvida para disponibilizar informações sobre a serie do Harry Potter

## Como utilizar a API

A API foi desenvolvida em docker então será necessário ter um docker instalado na máquina.

* Para baixar o docker: **https://docs.docker.com/compose/install/**
* Para fazer rodar a aplicação basta seguir os seguintes passos:
    - `docker-compose build`
    - `docker-compose up`

Rodando apenas esses comandos a API estará disponível em **127.0.0.1:8000** com um banco **Mysql** pronto para ser populado. =)

## Teste

Para rodar os testes da aplicação basta rodar o seguinte comando:

`docker-compose exec app adonis test functional`

## Documentação

* **URLs:**
    - GET - /harry-potter
    - POST - /harry-potter
    - PUT - /harry-potter/:id
    - DELETE - /harry-potter/:id
* **Métodos:**
    - `GET` | `POST` | `DELETE` | `PUT`
* **Parâmetros de URL:**
    - `house=[string]`