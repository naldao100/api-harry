FROM node:12

WORKDIR /app

COPY package.json package.json

EXPOSE 8000

RUN npm install pm2 -g && npm install

RUN npm i -g @adonisjs/cli nodemon

COPY ./services/start.sh /etc/start.sh

RUN chmod +x /etc/start.sh

CMD [ "/etc/start.sh", "dev" ]