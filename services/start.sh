#!/bin/bash

# generate the APP KEY with adonis
adonis key:generate

# generate all migration
adonis migration:run

# install all dependencies
npm i

#start serve
adonis serve --dev
